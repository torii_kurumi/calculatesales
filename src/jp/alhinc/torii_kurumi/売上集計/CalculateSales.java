package jp.alhinc.torii_kurumi.売上集計;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {

		try {
            //支店マップ
			HashMap<String, String> branchMap = new HashMap<>();
            //売上集計マップ
            HashMap<String, Long> salesMap = new HashMap<>();

			//branch.lstのファイルが存在しない場合エラーコードを出す
			File branchLst = new File(args[0], "branch.lst");
			if (!branchLst.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			//支店定義ファイルを読み込み
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(branchLst));

				//支店定義ファイルの1行ずつの読み込み、配列に並び替え
				String str;
				while((str = br.readLine()) != null) {
					String[] elements = str.split(",", -1);

					//支店定義ファイルのフォーマットを確認する
					if(elements.length != 2 || !elements[0].matches("^\\d{3}$") || elements[1].isEmpty()) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					//支店定義のファイル配列をそれぞれのマップに保持させる
					branchMap.put(elements[0], elements[1]);
					salesMap.put(elements[0], 0L);
				}
			} finally {
				br.close();
            }

			// リストを初期化し、ArrayListクラス(ファイルの名前一覧）を作成
			List<String> rcdFilenames = new ArrayList<>();
			File directory = new File(args[0]);             //allfileクラスの生成 全てのファイル読み込み
			for(String filename : directory.list()) {     //リスト分だけ繰り返す
				//新規オブジェクトの作成
				File file = new File(args[0], filename);

				//ファイル名を正規表現を使い"数字8桁かつrcd"を上のファイルリストに追加する
				if(filename.matches("^\\d{8}.rcd$") && file.isFile()) {
					rcdFilenames.add(filename);
				}
			}

			//上記コレクションをソートメソッドにより昇順に並び替える
			Collections.sort(rcdFilenames);

            //歯抜け確認 min maxの値を取得 rcdが邪魔をしているので、指定した範囲の文字列を切り出す
			int min = Integer.parseInt(rcdFilenames.get(0).substring(0, 8));
			int max = Integer.parseInt(rcdFilenames.get(rcdFilenames.size() -1).substring(0, 8));

			if(min + (rcdFilenames.size() -1) != max) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			br = null;
			for(String rcdFilename : rcdFilenames) {    //リスト分だけ繰り返しさせる
				try {
					br = new BufferedReader(new FileReader(new File(args[0], rcdFilename))); //ファイルの読み込み

                    //売上ファイルの1行目を読み込む(支店コード)
					String branchcode = br.readLine();

                    //上記が支店定義ファイルと一致しているか確認し、不一致の場合エラー
					if(!branchMap.containsKey(branchcode)) {
						System.out.println(rcdFilename + "の支店コードが不正です");
						return;
					}

                    //2行目を読み込み（売上額）
					Long salesamount = Long.parseLong(br.readLine());

                    //売上ファイルの3行目を読み込み、エラーメッセージ
					if(br.readLine() != null) {
						System.out.println(rcdFilename + "のフォーマットが不正です");
						return;
					}

					//売上マップからキーを使って値の金額を取り出す
					Long amount = salesMap.get(branchcode);

					//同じ支店コードのものは金額を足す
					Long totalamount = amount + salesamount;

					//10桁超えている場合エラーメッセージを出す
					if(totalamount.toString().length() > 10){
						System.out.println("合計金額が10桁超えました");
						return;
                    }

                    //上記のトータル、支店コードをマップに保持する
					salesMap.put(branchcode, totalamount);

				} finally {
					br.close();
				}
			}

			//branch.outへ書き込み準備
			BufferedWriter bw = null;
			try {
				File branchOut = new File(args[0], "branch.out");  //ファイルの読み込み
				bw = new BufferedWriter(new FileWriter(branchOut));  //brFileに文字型出力ストリームに書き込み

				//マップを変数に変更して一覧で取得する
				for (String key : branchMap.keySet()) {
                    //支店名
                    String branchValue = branchMap.get(key);
                    //売上金額
                    Long salesValue = salesMap.get(key);

					//上記をカンマで区切り、定義とする
					String definition = String.format("%s,%s,%s", key, branchValue, salesValue);

					//ファイルに書き出し
					bw.write(definition);
					bw.newLine();    //クローズさせるとループが終わるので改行文字をはさむ
				}
			}finally {
				bw.close();
			}
		} catch (Exception e) {
            //上記以外のエラーメッセージ
            System.out.println("予期せぬエラーが発生しました");
		}
	}
}
